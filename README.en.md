# tb_energy_monitoring_public

#### Description
    该平台是基于强大的IoT开源平台：ThingsBoard开发落地的能耗监控管理的解决方案，可轻松实现百万级设备接入，管理，数据采集与数据展示以及数据分析。平台已适配市面主流的计量设备串口通讯协议（DL/T645，CJ/T188等），并支持私有串口协议配置；同时方案还集成了低成本4G数据采集器，即插即用。多元化的数据可视化组件使得用户可以自由拼装符合自己需求的数据可视化看板，所见即所得。


#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
